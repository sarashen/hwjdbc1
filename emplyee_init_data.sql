-- --------------------------------------------------------
-- 主機:                           10.67.67.186
-- 伺服器版本:                        5.7.27 - MySQL Community Server (GPL)
-- 伺服器作業系統:                      Linux
-- HeidiSQL 版本:                  11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 傾印 sara 的資料庫結構
CREATE DATABASE IF NOT EXISTS `sara` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sara`;

-- 傾印  資料表 sara.employee 結構
CREATE TABLE IF NOT EXISTS `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `high` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `ename` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ext` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bmi` double DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;

-- 正在傾印表格  sara.employee 的資料：~17 rows (近似值)
DELETE FROM `employee`;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` (`id`, `high`, `weight`, `ename`, `name`, `ext`, `email`, `bmi`, `create_time`, `update_time`) VALUES
	(77, 181, 64, 'Edward-Liaw', '廖強原', '6120', 'edwardliaw@transglobe.com.tw', 19.54, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(78, 177, 80, 'Aaron-Huang', '黃智原', '2115', 'AaronHuang@transglobe.com.tw', 25.54, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(79, 154, 69, 'Aya-Wong', '黃大羚', '6144', 'ayawong@transglobe.com.tw', 29.09, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(80, 163, 53, 'Alvin-Chen', '陳美鴻', '6121', 'AlvinChen@transglobe.com.tw', 19.95, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(81, 182, 88, 'Amy-Hung', '洪毅芳', '6190', 'AmyHung@transglobe.com.tw', 26.57, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(82, 160, 88, 'Annie-Wang', '王雯妮', '6159', 'AnnieWang@transglobe.com.tw', 34.38, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(83, 189, 69, 'Cosmo-Hsueh', '薛安承', '6140', 'CosmoHsueh@transglobe.com.tw', 19.32, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(84, 175, 66, 'Disen-Chen', '陳祐森', '6139', 'disenchen@transglobe.com.tw', 21.55, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(85, 151, 58, 'Douglas-Chang', '張迪道', '6119', 'DouglasChang@transglobe.com.tw', 25.44, '2020-04-12 15:00:13', '2020-04-12 15:15:48'),
	(86, 165, 56, 'George-Gao', '高守緯', '6118', 'GeorgeGao@transglobe.com.tw', 20.57, '2020-04-12 15:00:13', '2020-04-12 15:15:48');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
