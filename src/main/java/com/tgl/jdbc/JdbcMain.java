package com.tgl.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.tgl.jdbc.Employee;

public class JdbcMain {

	private static String url = "jdbc:mysql://10.67.67.186:3306/sara?useUnicode=true&characterEncoding=utf8";
	private static String user = "root";
	private static String password = "123456";

	public static void main(String[] arg) {

		try (Connection con = DriverManager.getConnection(url, user, password)) {

			insertPre(con); // prepare insert function data
			updatePre(con); // prepare Update function data
			deletePre(con); // prepare inquiry function data
			inquiryPre(con); // prepare inquiry function data

			con.close();

		} catch (SQLException e) {

		}

	} // main

	/**
	 * Prepare insert data for employee(可多筆)
	 * 
	 * @param con : connection
	 */
	public static void insertPre(Connection con) {

		List<Employee> emp = new ArrayList<>();
		DecimalFormat df = new DecimalFormat("##.00");
		System.out.println("1.---- insert into to employee table -----");
		EmployeeFunction employeeFunction = new EmployeeFunction();
		int id = 0;
		int high = 160;
		int weigh = 55;
		String ename = "TEST ADD";
		String name = "開心小鬼";
		String ext = "6955";
		String email = "happyhsiao@transglobe.com.tw";
		Float bmi1 = calcBmi(high, weigh);

		Employee s1 = new Employee(id, high, weigh, ename, name, ext, email, bmi1);
		emp.add(s1);
		id = 0;
		high = 175;
		weigh = 88;
		ename = "Kevin Chen";
		name = "開心大鬼";
		ext = "6938";
		email = "chenKevin@transglobe.com.tw";

		bmi1 = calcBmi(high, weigh);
		Employee s2 = new Employee(id, high, weigh, ename, name, ext, email, bmi1);
		emp.add(s2);

		employeeFunction.createData(con, emp);

		System.out.println("1.---- insert employee table Successful -----");

	} // end insertPre

	/**
	 * Prepare update data for employee--by id update data
	 * 
	 * @param con :connection
	 */

	public static void updatePre(Connection con) {

		List<Employee> emp = new ArrayList<>();
		System.out.println("2.---- Update employee table --Start---");

		EmployeeFunction employeeFunction = new EmployeeFunction();

		int id = 80; // update ID 60 data
		int high = 160;
		int weigh = 55;
		String ename = "TEST Update";
		String name = "小精靈";
		String ext = "1234";
		String email = "happytest@transglobe.com.tw";
		Float bmi1 = calcBmi(high, weigh);

		Employee s1 = new Employee(id, high, weigh, ename, name, ext, email, bmi1);
		emp.add(s1);

		id = 100; // ID 100 不存在時的處理
		high = 175;
		weigh = 88;
		ename = "Joey Wang";
		name = "傷心人";
		ext = "6938";
		email = "chenKevin@transglobe.com.tw";
		bmi1 = calcBmi(high, weigh);
		Employee s2 = new Employee(id, high, weigh, ename, name, ext, email, bmi1);
		emp.add(s2);

		employeeFunction.updateData(con, emp);

		System.out.println("2.---- Update employee table ---end--");

	} // end Update

	/**
	 * Prepare delete data for employee--可依ID或EXTENTION刪除
	 * 
	 * @param con : connection
	 */
	public static void deletePre(Connection con) {

		DecimalFormat df = new DecimalFormat("##.00");
		List<Employee> emp = new ArrayList<>();
		System.out.println("----Delete  employee table --Start---");

		EmployeeFunction employeeFunction = new EmployeeFunction();

		int id = 78; // delete ID 78 data
		int high = 0, weigh = 0;
		String name = null, email = null;
		String ext = "6159"; // delete extention = 6159(ID=82) data
		String ename = "HANERY";

		Float bmi = (float) 0;

		Employee s1 = new Employee(id, high, weigh, ename, name, ext, email, bmi);
		emp.add(s1);
		System.out.println("3.----Kill ID-->" + id);
		employeeFunction.deleteData(con, emp, Employee.EnumField.ID);
		System.out.println("3.----Kill Extension-->" + ext);
		employeeFunction.deleteData(con, emp, Employee.EnumField.EXTENSION);
		System.out.println("3.----Kill ENAME -->" + ename);
		employeeFunction.deleteData(con, emp, Employee.EnumField.ENAME);

		System.out.println("---- Delete employee table ---end--");

	} // end Update

	/**
	 * Prepare inquery employee--可依ID,身高, 體重, BMI & 升幂或降幂 查詢
	 * 
	 * @param co
	 */
	public static void inquiryPre(Connection co) {

		EmployeeFunction employeeFunction = new EmployeeFunction();

		System.out.println("---- Inquiry employee table -----");

		System.out.println("4.----Inquery By ID  DESC ----");
		employeeFunction.inquiryData(co, Employee.EnumField.ID, Employee.EnumOrder.DESC);
		System.out.println("4.----Inquery By High ASC ----");
		employeeFunction.inquiryData(co, Employee.EnumField.HIGH, Employee.EnumOrder.ASC);

	} // end inquiry

	/**
	 * Caculate BMI = weigh / (high)^2 (m)
	 * 
	 * @param high   : 身高(cm)
	 * @param weigh: 體重(kg)
	 * @return : Float BMI
	 */
	public static Float calcBmi(int high, int weigh) {
		DecimalFormat df = new DecimalFormat("##.00");
		Float high1 = (float) high / 100;
		Float weigh1 = (float) weigh;
		Float bmi = (float) (weigh1 / (high1 * high1));
		Float bmi1 = (float) Double.parseDouble(df.format(bmi));
		return bmi1;
	} // end calcBmi
} // class
