package com.tgl.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class EmployeeFunction {
	/**
	 * insert data to employee
	 */
	public void createData(Connection co, List<Employee> emp) {

		try {
			String sql = "INSERT INTO employee (high, weight, ename, name, ext, email, bmi) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement stmt = co.prepareStatement(sql);

			for (Employee e : emp) {
				stmt.setInt(1, e.getHigh());
				stmt.setInt(2, e.getWeight());
				stmt.setString(3, e.getEname());
				stmt.setString(4, e.getName());
				stmt.setString(5, e.getExt());
				stmt.setString(6, e.getEmail());
				stmt.setDouble(7, e.getBmi());

				stmt.executeUpdate();
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // tru
			// return ;
	} // createData

	/***
	 * Update Employee Data
	 * 
	 * @param co  : connection
	 * @param emp : according ID , update another field
	 */
	public void updateData(Connection co, List<Employee> emp) {

		try {
			String sql = "UPDATE employee SET HIGH=? ,WEIGHT=? ,ENAME=? ,NAME=? ,EXT=? ,EMAIL=? ,BMI=? WHERE ID=? ";
			PreparedStatement stmt = co.prepareStatement(sql);
			int rt;
			for (Employee e : emp) {
				stmt.setInt(1, e.getHigh());
				stmt.setInt(2, e.getWeight());
				stmt.setString(3, e.getEname());
				stmt.setString(4, e.getName());
				stmt.setString(5, e.getExt());
				stmt.setString(6, e.getEmail());
				stmt.setDouble(7, e.getBmi());
				stmt.setLong(8, e.getId());
				System.out.print("Update id--->" + e.getId());
				rt = stmt.executeUpdate();
				if (rt == 1) {
					System.out.println("   Successful");
				} else {
					System.out.println("   data not found ");
				}
			}
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // catch
	} // updateDate

	/**
	 * 依據field 傳入的欄位及存在EMP之欄位之值進行刪除
	 * 
	 * @param co    : input connection
	 * @param emp   : delete data
	 * @param field : delete key field
	 */

	public void deleteData(Connection co, List<Employee> emp, Employee.EnumField field) {

		StringBuilder sb = new StringBuilder();
		PreparedStatement stmt;

		try {
			for (Employee e : emp) {
				sb.delete(0, sb.length());
				sb.append("DELETE FROM employee WHERE ");
				switch (field) {
				case ID:
					sb.append(" ID = ? ");
					stmt = co.prepareStatement(sb.toString());
					stmt.setLong(1, e.getId());
					stmt.executeUpdate();
					break;
				case EXTENSION:
					sb.append(" EXT =  ? ");
					stmt = co.prepareStatement(sb.toString());
					stmt.setString(1, e.getExt());
					stmt.executeUpdate();
					break;
				default:
					System.out.println("DELETE PARAMETER ERROR");
					break;
				} // switch
			} // end for

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // catch

	} // deleteData

	/**
	 * Function : employee table Inquiry
	 * 
	 * @param co    : connection
	 * @param field : Inquiry Field
	 * @param order : Order by Ascending / Descending
	 * @throws SQLException
	 */
	public void inquiryData(Connection co, Employee.EnumField field, Employee.EnumOrder order) {

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT * FROM employee ORDER BY ");

		switch (field) {
		case ID:
			sb.append(" ID ");
			break;
		case HIGH:
			sb.append(" HIGH ");
			break;
		case WEIGHT:
			sb.append(" WEIGHT ");
			break;
		case BMI:
			sb.append(" BMI ");
			break;
		default:
			sb.append(" ID ");
			break;
		} // switch

		switch (order) {
		case ASC:
			sb.append(" ASC ");
			break;
		case DESC:
			sb.append(" DESC ");
			break;
		default:
			sb.append(" ASC ");
			break;
		} // switch order

		PreparedStatement stmt;
		try {
			stmt = co.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				System.out.print("id->" + rs.getInt(1));
				System.out.print(" ,high->" + rs.getInt(2));
				System.out.print(" ,weight->" + rs.getInt(3));
				System.out.print(" ,english_name->" + rs.getString(4));
				String str = maskutil(rs.getString(5));
				System.out.print(" ,name->" + str);
				System.out.print(" ,extension->" + rs.getString(6));
				System.out.print(" ,email->" + rs.getString(7));
				System.out.print(" ,bmi->" + rs.getDouble(8));
				System.out.print(" ,Create_time->" + rs.getDate(9));
				System.out.println(" ,Update_time->" + rs.getDate(10));
			} // end while
			stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Inquiry data End--->");
	} // inquiryData

	public String maskutil(String input) {
		if (input.isEmpty()) {
			return null;
		}

		StringBuilder sb = new StringBuilder();

		sb.append(input.charAt(0));
		sb.append('X');
		sb.append(input.substring(2));
		return sb.toString();
	} // maskutil end

} // EmployeeFunction
